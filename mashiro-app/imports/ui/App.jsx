import { Meteor } from 'meteor/meteor'
import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import RaisedButton from 'material-ui/RaisedButton';
import AppBar from 'material-ui/AppBar';
import { List } from 'material-ui/List';
import Divider from 'material-ui/Divider';
import { createContainer } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom'

// Database collection
import { Waifus } from '../../lib/waifus';

import Waifu from './Waifu';
import Edit from './EditWaifu'
import TeamList from './Team-list';
import TeamStats from './Team-stats';
import AccountsWrapper from './AccountsWrapper';

// What's shown when no waifu is clicked yet
const iconicWaifu = {
    name: "Shiina Mashiro",
    anime: "Sakurasou no Pet na Kanojo",
    drawing: 10,
    singing: "?",
    playingInstruments: "?",
    memorizing: 10,
    socializing: 1,
    cooking: 3,
    notes: "Iconic Waifu!",
}

export class App extends Component {
    constructor(props) {
        super(props);

        // Setting up the state
        this.state = {
            currentWaifu: iconicWaifu,
            showEditWaifu: false,
        };

        // Binding function
        this.updateCurrentWaifu = this.updateCurrentWaifu.bind(this);
        this.showEditForm = this.showEditForm.bind(this);
        this.showTeamStats = this.showTeamStats.bind(this);
    }

    updateCurrentWaifu(waifu) {
        Object.keys(waifu).map(function (index) {
            console.log(waifu[index]);
            if (waifu[index] == 11) {
                return (waifu[index] = "?")
            } else {
                return waifu[index];
            }
        });

        this.setState({
            currentWaifu: waifu,
        })
    }

    renderWaifus() {
        return this.props.waifus.map((waifu) => (
            <TeamList key={waifu._id} waifu={waifu} updateCurrentWaifu={this.updateCurrentWaifu} />
        ))
    }

    showEditForm() {
        this.setState({
            showEditWaifu: true,
        });
    }

    showTeamStats() {
        this.setState({
            showEditWaifu: false,
        });
    }

    showForm() {
        if (this.state.showEditWaifu === true) {
            return (<Edit currentWaifu={this.state.currentWaifu}
                showTeamStats={this.showTeamStats} />
            );
        } else {
            console.log(this.props.waifus);
            return (
                <TeamStats waifus={this.props.waifus}/>
            );
        }
    }

    render() {
        return (
            <MuiThemeProvider>
                <div className="container">
                    <AppBar title="Who-knows-what Application"
                        iconClassNameRight="muidocs-icon-natigation-expand-more"
                        showMenuIconButton={false}
                        style={{backgroundColor: '#0277BD'}}>
                        <AccountsWrapper />
                    </AppBar>
                    <div className="row">
                        <div className="col s12 m7"><Waifu waifu={this.state.currentWaifu} showEditForm={this.showEditForm} /></div>
                        <div className="col s12 m5">
                            <h2>Team List</h2>
                            <Link to="/new" className="waves-effect waves-light btn light-blue darken-3">Add new waifu</Link>
                            <List>
                                {this.renderWaifus()}
                            </List>
                            <Divider />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col s12">
                            <br />
                            <Divider />
                            {this.showForm()}
                            <Divider />
                        </div>
                    </div>
                </div>
            </MuiThemeProvider>
        )

    }
}

App.propTypes = {
    waifus: PropTypes.array.isRequired,
};

export default createContainer(() => {
    Meteor.subscribe('waifus');
    const user = Meteor.userId();

    return {
        waifus: Waifus.find({ owner: user }, { sort: { name: 1 } }).fetch(),
    };
}, App); // Container tied to App