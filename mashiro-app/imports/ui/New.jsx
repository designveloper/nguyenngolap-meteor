import { Meteor } from 'meteor/meteor';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import {GridList, GridTile} from 'material-ui/GridList';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import Point from "../../lib/pointSystem";
import Waifus from "../../lib/waifus";

const styles = {
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 100,
    }
}

export default class New extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            anime: "",
            drawing: null,
            singing: null,
            playingInstruments: null,
            memorizing: null,
            socializing: null,
            cooking: null,
            notes: "",
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
        this.submitWaifu = this.submitWaifu.bind(this);
    }

    static propTypes = {
        history: PropTypes.object.isRequired
    }

    submitWaifu(event) {
        event.preventDefault();

        let waifu = {
            name: this.state.name,
            anime: this.state.anime,
            drawing: this.state.drawing,
            singing: this.state.singing,
            playingInstruments: this.state.playingInstruments,
            memorizing: this.state.memorizing,
            socializing: this.state.socializing,
            cooking: this.state.cooking,
            notes: this.state.notes,
            createdAt: new Date(),
            owner: Meteor.userId(),
        };

        Meteor.call('insertWaifu', waifu, (error) => {
            if (error) {
                Meteor.call("logToConsole", error);
                alert("Oops! Something is wrong: " + error.reason);
            } else {
                alert("New waifu added!");
                this.props.history.push('/');
            }
        });
    }

    // Default arguments: event, index, value (implicit?)
    // We're interacting through tap event plugin, so we can't access the real props
    // To get the props value, we have to pass it in as arguments
    handleChange(id, event, value) {
        var newState = { ...this.state }
        newState[id] = value + 1;
        this.setState(newState);
    };

    handleTextChange(id, event) {
        var newState = { ...this.state }
        newState[id] = event.target.value;
        this.setState(newState);
    };

    scores = [
        <MenuItem key={1} value={1} primaryText={Point[1]} />,
        <MenuItem key={2} value={2} primaryText={Point[2]} />,
        <MenuItem key={3} value={3} primaryText={Point[3]} />,
        <MenuItem key={4} value={4} primaryText={Point[4]} />,
        <MenuItem key={5} value={5} primaryText={Point[5]} />,
        <MenuItem key={6} value={6} primaryText={Point[6]} />,
        <MenuItem key={7} value={7} primaryText={Point[7]} />,
        <MenuItem key={8} value={8} primaryText={Point[8]} />,
        <MenuItem key={9} value={9} primaryText={Point[9]} />,
        <MenuItem key={10} value={10} primaryText={Point[10]} />,
        <MenuItem key={11} value={11} primaryText={Point[11]} />,
    ];

    render() {
        // Check the documentation on this
        return (
            <MuiThemeProvider>
                <div style={styles.root}>
                    <form className="col s12" onSubmit={this.submitWaifu}>
                        <h3>Add a new waifu</h3>
                        <GridList
                            cellHeight={120}
                        >
                            <GridTile>
                                <h5>Name</h5>
                                <TextField
                                    value={this.state.name}
                                    onChange={(event, value) => this.handleTextChange("name", event, value)}
                                    hintText="Name"
                                />
                            </GridTile>

                            <GridTile>
                                <h5>Anime</h5>
                                <TextField
                                    value={this.state.anime}
                                    onChange={(event, value) => this.handleTextChange("anime", event, value)}
                                    hintText="Anime"
                                />
                            </GridTile>

                            <GridTile>
                                <h5>Drawing</h5>
                                <SelectField
                                    value={this.state.drawing}
                                    onChange={(event, value) => this.handleChange("drawing", event, value)}
                                    hintText="Select your assessment"
                                >
                                    {this.scores}
                                </SelectField>
                            </GridTile>

                            <GridTile>
                                <h5>Singing</h5>
                                <SelectField
                                    value={this.state.singing}
                                    onChange={(event, value) => this.handleChange("singing", event, value)}
                                    hintText="Select your assessment"
                                >
                                    {this.scores}
                                </SelectField>
                            </GridTile>

                            <GridTile>
                                <h5>Playing instruments</h5>
                                <SelectField
                                    value={this.state.playingInstruments}
                                    onChange={(event, value) => this.handleChange("playingInstruments", event, value)}
                                    hintText="Select your assessment"
                                >
                                    {this.scores}
                                </SelectField>
                            </GridTile>

                            <GridTile>
                                <h5>Memorizing</h5>
                                <SelectField
                                    value={this.state.memorizing}
                                    onChange={(event, value) => this.handleChange("memorizing", event, value)}
                                    hintText="Select your assessment"
                                >
                                    {this.scores}
                                </SelectField>
                            </GridTile>

                            <GridTile>
                                <h5>Socializing</h5>
                                <SelectField
                                    value={this.state.socializing}
                                    onChange={(event, value) => this.handleChange("socializing", event, value)}
                                    hintText="Select your assessment"
                                >
                                    {this.scores}
                                </SelectField>
                            </GridTile>

                            <GridTile>
                                <h5>Cooking</h5>
                                <SelectField
                                    value={this.state.cooking}
                                    onChange={(event, value) => this.handleChange("cooking", event, value)}
                                    hintText="Select your assessment"
                                >
                                    {this.scores}
                                </SelectField>
                            </GridTile>

                            <GridTile>
                                <h5>Notes</h5>
                                <TextField
                                    value={this.state.notes}
                                    onChange={(event, value) => this.handleTextChange("notes", event, value)}
                                    hintText="Notes"
                                    multiLine={true}
                                    rowsMax={2}
                                />
                            </GridTile>

                            <GridTile>
                                <button className="btn waves-effect waves-light light-blue darken-3" type="submit" name="action">Submit
                                <i className="material-icons right">send</i>
                                </button>
                            </GridTile>

                        </GridList>
                    </form>
                </div>
            </MuiThemeProvider>
        )
    };
}