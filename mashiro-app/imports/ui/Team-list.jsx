import React from 'react';
import { Component } from 'react';
import Avatar from 'material-ui/Avatar';
import { ListItem } from 'material-ui/List';
import ActionDeleteForever from 'material-ui/svg-icons/action/delete-forever'
import { red500 } from 'material-ui/styles/colors'

export default class TeamList extends Component {
    // Define the function locally
    updateCurrentWaifu(waifu) {
        this.props.updateCurrentWaifu(waifu);
    }

    deleteWaifu(waifuId) {
        Meteor.call('deleteWaifu', waifu, (error) => {
            if (error) {
                Meteor.call("logToConsole", error);
                alert("Oops! Something is wrong: " + error.reason);
            } else {
                alert("Waifu deleted!");

            }
        })
    }

    // The onClick method: Pass the current waifu as an argument
    // to the local updateCurrentWaifu function, then this function
    // will call the updateCurrentWaifu that App passed in
    render() {
        return (
            <ListItem 
                primaryText={this.props.waifu.name}
                leftAvatar={<Avatar src="mashiro.jpg" />}
                rightIcon={<ActionDeleteForever hoverColor={red500} 
                    onClick={this.deleteWaifu.bind(this, this.props.waifu._id)}
                />}
                onClick={this.updateCurrentWaifu.bind(this, this.props.waifu)}
            />
        )
    }
}
