import React from 'react';
import { Component } from 'react';
import { Card, CardMedia, CardTitle, CardText, CardActions } from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import Avatar from 'material-ui/Avatar'
import Chip from 'material-ui/Chip'
import { blue200, lightBlue800, lightBlue50 } from 'material-ui/styles/colors'

const styles = {
    chip: {
        margin: 4,
    },

    wrapper: {
        overflow: "auto",
    },

    card: {
        display: "inline-block",
        float: "left",
    },

    button: {
        margin: 12,
    },
}

export default class Waifu extends Component {
    showEditForm() {
        this.props.showEditForm();
    }

    render() {
        return (
            <Card>
                <CardMedia
                    overlay={<CardTitle title={this.props.waifu.name} subtitle={this.props.waifu.notes} />}
                >
                    <img src="mashiro.jpg" alt="" />
                </CardMedia>
                <CardText style={styles.wrapper}>
                    <div style={styles.card}>
                        <Chip
                            backgroundColor={blue200}
                            style={styles.chip}
                        >
                            <Avatar size={32} color={lightBlue50} backgroundColor={lightBlue800}>
                                {this.props.waifu.drawing}
                            </Avatar>
                            Drawing
                        </Chip>
                    </div>
                    <div style={styles.card}>
                        <Chip
                            backgroundColor={blue200}
                            style={styles.chip}
                        >
                            <Avatar size={32} color={lightBlue50} backgroundColor={lightBlue800}>
                                {this.props.waifu.singing}
                            </Avatar>
                            Singing
                        </Chip>
                    </div>
                    <div style={styles.card}>
                        <Chip
                            backgroundColor={blue200}
                            style={styles.chip}
                        >
                            <Avatar size={32} color={lightBlue50} backgroundColor={lightBlue800}>
                                {this.props.waifu.playingInstruments}
                            </Avatar>
                            Playing instruments
                        </Chip>
                    </div>
                    <div style={styles.card}>
                        <Chip
                            backgroundColor={blue200}
                            style={styles.chip}
                        >
                            <Avatar size={32} color={lightBlue50} backgroundColor={lightBlue800}>
                                {this.props.waifu.memorizing}
                            </Avatar>
                            Memorizing
                        </Chip>
                    </div>
                    <div style={styles.card}>
                        <Chip
                            backgroundColor={blue200}
                            style={styles.chip}
                        >
                            <Avatar size={32} color={lightBlue50} backgroundColor={lightBlue800}>
                                {this.props.waifu.socializing}
                            </Avatar>
                            Socializing
                        </Chip>
                    </div>
                    <div style={styles.card}>
                        <Chip
                            backgroundColor={blue200}
                            style={styles.chip}
                        >
                            <Avatar size={32} color={lightBlue50} backgroundColor={lightBlue800}>
                                {this.props.waifu.cooking}
                            </Avatar>
                            Cooking
                        </Chip>
                    </div>
                </CardText>
                <CardActions>
                    <RaisedButton 
                        label="Edit waifu/stats"
                        labelPosition="before"
                        style={styles.button}
                        onClick={this.showEditForm.bind(this)}
                    />
                </CardActions>
            </Card>
        )
    }
}
