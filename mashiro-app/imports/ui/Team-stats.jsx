import React from 'react';
import { Component } from 'react';
import { Radar } from 'react-chartjs-2';
import Divider from 'material-ui/Divider';

export default class TeamStats extends Component {
    render() {
        const waifus = this.props.waifus;
        const numWaifus = waifus.length;

        // const team = get a total for a skill
        // const possible = 10 * numWaifus
        // Can just use your own formula

        // const drawing = Math.round((waifus.reduce((drawing, waifu) => {
        //     return drawing + waifu.drawing;
        // }, 0) / (10 * numWaifus)) * 100);
        // const singing = Math.round((waifus.reduce((singing, waifu) => {
        //     return singing + waifu.singing;
        // }, 0) / (10 * numWaifus)) * 100);
        // const playingInstrumentsplayingInstruments = Math.round((waifus.reduce((playingInstruments, waifu) => {
        //     return playingInstruments + waifu.playingInstruments;
        // }, 0) / (10 * numWaifus)) * 100);
        // const memorizing = Math.round((waifus.reduce((memorizing, waifu) => {
        //     return memorizing + waifu.memorizing;
        // }, 0) / (10 * numWaifus)) * 100);
        // const socializing = Math.round((waifus.reduce((socializing, waifu) => {
        //     return socializing + waifu.socializing;
        // }, 0) / (10 * numWaifus)) * 100);
        // const cooking = Math.round((waifus.reduce((cooking, waifu) => {
        //     return cooking + waifu.cooking;
        // }, 0) / (10 * numWaifus)) * 100);

        // Put the calculated nums above to the data (not now)
        const data = {
            labels: ['Drawing', 'Singing', 'Playing instruments', 'Memorizing', 'Socializing', 'Cooking'],
            datasets: [
                {
                    label: 'In % of max possible',
                    backgroundColor: 'rgba(143, 202, 249, 0.2)',
                    borderColor: 'rgba(12, 71, 161, 1)',
                    pointBackgroundColor: 'rgba(12, 71, 161, 1)',
                    pointBorderColor: '#fff',
                    pointHoverBackgroundColor: '#fff',
                    pointHoverBorderColor: 'rgba(12, 71, 161, 1)',
                    data: [65, 59, 90, 81, 56, 55, 40]
                },
            ],
        };

        // You can insert the right side data here
        return (
            <div>
                <h1>Team Stats</h1>
                <div className="row">
                    <div className="col s12 m7">
                        <Radar
                            data={data}
                            width={500}
                            height={500}
                            option={{
                                maintainAspectRatio: false
                            }}
                        />
                    </div>
                    <div className="col s12 m5">
                        <h4>Scroes in % of max possibles</h4>
                        <Divider />
                        <h4>Bla bla bla...</h4>
                        <Divider />
                        <h4>Bla bla bla...</h4>
                    </div>
                </div>
            </div>
        );
    }
}
