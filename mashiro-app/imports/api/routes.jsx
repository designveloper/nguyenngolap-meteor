import React, { Component } from 'react';
import {BrowserRouter, Route, Switch } from 'react-router-dom';

import App from '../ui/App';
import New from '../ui/New';
import Lost from '../ui/Lost';

export default class Router extends Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" component={App} />
                    <Route exact path="/new" component={New} />
                    <Route exact path="*" component={Lost} />
                </Switch>
            </BrowserRouter>
        )
    }
}