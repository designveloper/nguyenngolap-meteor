import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';

import Router from '../imports/api/routes';

// Inject to the application
injectTapEventPlugin();

Meteor.startup(() => {
    render(
        <Router />,
        document.getElementById('render-target')
    );
})
