export default Point = {
    1: "1 point",
    2: "2 points",
    3: "3 points",
    4: "4 points",
    5: "5 points",
    6: "6 points",
    7: "7 points",
    8: "8 points",
    9: "9 points",
    10: "10 points",
    11: "? - Not sure"
}