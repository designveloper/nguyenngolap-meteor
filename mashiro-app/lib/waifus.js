import { Mongo } from 'meteor/mongo';

export const Waifus = new Mongo.Collection('waifus');

Waifus.allow({
    insert() { return false; },
    update() { return false; },
    remove() { return false; }
});

Waifus.deny({
    insert() { return true; },
    update() { return true; },
    remove() { return true; }
})

const WaifuSchema = new SimpleSchema({
    // To prevent the user from not calling a certain field,
    // causing error, we use defaultValue
    _id: { type: String },
    name: { type: String },
    anime: { type: String },
    drawing: { type: String, defaultValue: 1 },
    singing: { type: String, defaultValue: 1 },
    playingInstruments: { type: String, defaultValue: 1 },
    memorizing: { type: String, defaultValue: 1 },
    socializing: { type: String, defaultValue: 1 },
    cooking: { type: String, defaultValue: 1 },
    // We don't have to have data for a field at all times
    notes: { type: String, optional: true },
    owner: { type: String },
})

Waifus.attachSchema(WaifuSchema);