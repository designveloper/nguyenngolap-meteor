import { Meteor } from 'meteor/meteor';
import { Waifus } from '../lib/waifus';

Meteor.methods({
    insertWaifu(waifu) {
        Waifus.insert(waifu);
    },

    updateWaifu(waifu) {
        Waifus.update(waifu._id,
        { $set: waifu });
    },

    deleteWaifu(waifuId) {
        Waifus.remove(waifuId);
    },

    logToConsole(msg) {
        console.log(msg);
    },
});