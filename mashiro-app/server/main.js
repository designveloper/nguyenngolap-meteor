import { Meteor } from 'meteor/meteor';
import { Waifus } from '../lib/waifus';

Meteor.startup(() => {
	// code to run on server at startup
	Meteor.publish('waifus', function () {
		var result = Waifus.find({});
		// console.log(result);
		return result;
	})
});
