# Meteor
Intern: Nguyen Ngo Lap

## 2. React Refresher: Basic Player View
2 types of component: Stateful and stateless (smart vs dumb component)

- Stateless: Doesn't do much on its own, and needs parent component to do something

### Materialize UI
> "A UI component library created with CSS, JavaScript, and HTML. Materialize UI components helps in constructing attractive, consistent, and functional web pages and web apps while adhering to modern web design principles like browser portability, device independence, and graceful degradation. It helps in creating faster, beautiful, and responsive websites. It is inspired from Google Material Design."

> \- *Tutorialspoint*

Reference:
- http://materializecss.com/about.html
- https://github.com/google/material-design-icons

### File structure for React and Meteor
**Methods:**

\- `Meteor.startup`: A method that can run anywhere in the project (Put in the client -> run in the client. Put in the server -> run in the server)

- The function will run as soon as the DOM is ready
    
> "Meteor.startup actually runs when all the files have completely downloaded from the server (javascript files). If you place your code to run at startup without putting it in a Meteor.startup it may not run because it would run where the JS/html has not been fully downloaded yet
>
> This is when the 'DOM is ready', but not necessarily when your HTML is rendered, because this (the HTML) renders when the DOM is ready too."
>
> https://stackoverflow.com/questions/23947621/meteor-startupfunc-when-is-the-dom-ready-in-meteor

**Notes:**

- When React returns a list from mapping method, it's looking for a unique key per object*
- State = snapshot of the app at a time
- Reference:
    - MaterializeCSS Grid: http://materializecss.com/grid.html

## 3. Reduce player stats
### Yarn
A package installer like NPM, uses package.json.

- Faster with a lot of versioning dependencies and security
- Built in collaboration with the community and FB, Google
*- Personal opinion: Currently shouldn't be used with Meteor on Windows*

### Blaze

- https://github.com/meteor/blaze
-  A powerful library for creating user interfaces by writing reactive HTML templates
- Each new Meteor project created has Blaze included

**Notes:**

- There're many changes in api behavior in `react-router` V4. Reference: https://reacttraining.com/react-router/web/api/
- Passing additional parameters with an onChange event: https://stackoverflow.com/questions/44917513/passing-an-additional-parameter-with-an-onchange-event
- To insert an object into the database, we need the code to run on the server (import/define the method in server side or put into `lib` folder?)
- The meteor package `accounts-ui` is not built for React. Need to wrap it in a Blaze template
- User Accounts packages ref: https://github.com/meteor-useraccounts

## 4. Players CRUD and Team View
### Chart.js
https://github.com/jerairrest/react-chartjs-2

